
# Known issues
Asio contains a bug in v1.18.1. This fix has not yet been release, but add inline to the template specialisation if you get dummy_return duplicate symbols on msvc.

# Requirements
- Qt 5.15 
- Protobuf 3
- Boost asio 1.75
- spdlog
- palsigslot