#pragma once

#include <ui_mainwindow.h>
#include <QComboBox>
#include <widgets/textentrypopup.h>

namespace Ui
{
struct MainWindow;
} // namespace Ui

namespace Ui
{
struct MainWindowExtra : public Ui::MainWindow
{
    QWidget *         toolbarSpacer;
    QComboBox *       deviceBox;
    QAction *         actionAddDevice;
    ::TextEntryPopup *networkPopup;

    void setupUi(QMainWindow *MainWindow)
    {
        MainWindow::setupUi(MainWindow);

        toolbarSpacer = new QWidget(toolBar);
        toolbarSpacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        deviceBox = new QComboBox(toolBar);
        deviceBox->setObjectName(QString::fromUtf8("deviceSelectCmbbox"));
        deviceBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        deviceBox->setMinimumSize(QSize(250, 0));

        actionAddDevice = new QAction(toolBar);
        actionAddDevice->setObjectName(QString::fromUtf8("actionAddDevice"));
        actionAddDevice->setCheckable(false);
        actionAddDevice->setAutoRepeat(false);
        actionAddDevice->setText(QString::fromUtf8("+"));
        actionAddDevice->setMenuRole(QAction::ApplicationSpecificRole);

        toolBar->addWidget(toolbarSpacer);
        toolBar->addWidget(deviceBox);
        toolBar->addAction(actionAddDevice);

        networkPopup = new ::TextEntryPopup(toolBar);
        networkPopup->setLabel(QString::fromUtf8("Address:"));
    }


    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow::retranslateUi(MainWindow);

        networkPopup->setLabel(QCoreApplication::translate("MainWindow", "Address:", nullptr));
    }
};
} // namespace Ui