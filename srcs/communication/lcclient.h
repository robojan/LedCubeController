#pragma once

#include <memory>
#include <asio.hpp>
#include <sigslot/signal.hpp>

class NetworkConnection;


class LCClient : std::enable_shared_from_this<LCClient>
{
public:
    LCClient(asio::io_context &context);
    LCClient(asio::io_context &context, std::shared_ptr<NetworkConnection> connection);
    LCClient(const LCClient &) = delete;
    ~LCClient();

    LCClient &operator=(const LCClient &) = delete;

    inline bool hasConnection() const { return static_cast<bool>(_conn); }

    void disconnect();

    inline const std::string &serial() const { return _serial; }

    // Signals
    sigslot::signal<LCClient *> sigDisconnected;

private:
    asio::io_context &                 _ioContext;
    std::shared_ptr<NetworkConnection> _conn;
    std::string                        _serial;
    asio::steady_timer                 _heartBeatTimer;

    void startAliveStateChecking();

    asio::awaitable<bool> doAliveChecking();
    void                  onAliveCheckingCallback(std::exception_ptr ex, bool result);
};
