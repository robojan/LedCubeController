
#include "lcclient.h"
#include "networkconnection.h"
#include <chrono>
#include <spdlog/spdlog.h>
#include "errors.h"

#include <led-cube.pb.h>

LCClient::LCClient(asio::io_context &context) : _ioContext(context), _heartBeatTimer(_ioContext) {}

LCClient::LCClient(asio::io_context &context, std::shared_ptr<NetworkConnection> connection)
    : _ioContext(context), _conn(std::move(connection)), _heartBeatTimer(_ioContext)
{
    startAliveStateChecking();
}

LCClient::~LCClient()
{
    _heartBeatTimer.cancel();
}

void LCClient::disconnect()
{
    if(hasConnection())
    {
        _conn.reset();
        sigDisconnected(this);
    }
}

void LCClient::startAliveStateChecking()
{
    asio::co_spawn(_ioContext, doAliveChecking(),
                   std::bind(&LCClient::onAliveCheckingCallback, this, std::placeholders::_1,
                             std::placeholders::_2));
}


asio::awaitable<bool> LCClient::doAliveChecking()
{
    using namespace std::chrono_literals;
    auto wp = weak_from_this();
    while(true)
    {
        // Every iteration recheck if the object still exists
        auto self = wp.lock();
        if(!self)
            break;
        ledcube::Request echoRequest;
        echoRequest.set_request(ledcube::Request_RequestId_Echo);
        echoRequest.mutable_echo()->set_serial("Controller");
        try
        {
            auto response = co_await _conn->request(echoRequest);
            if(!response.has_echo())
            {
                throw ProtocolError("Unexpected response to an echo request");
            }
        }
        catch(const asio::system_error &er)
        {
            if(er.code() != asio::error::operation_aborted)
                throw;
            // No response from LED Cube
            co_return false;
        }
        _heartBeatTimer.expires_after(5s);
        co_await _heartBeatTimer.async_wait(asio::use_awaitable);
    }
    co_return true;
}

void LCClient::onAliveCheckingCallback(std::exception_ptr ex, bool result)
{
    try
    {
        if(ex)
            std::rethrow_exception(ex);
        if(!result)
        {
            spdlog::warn("LED Cube did not send an heart-beat reply");
            disconnect();
        }
    }
    catch(const ErrorMsgReceived &error)
    {
        spdlog::error("Received error on echo request: {}", error.msg);
        disconnect();
    }
    catch(const ProtocolError &ex)
    {
        spdlog::error("Protocol error: {}", ex.what());
        disconnect();
    }
    catch(const InvalidMsg &ex)
    {
        spdlog::error("Invalid message error: {}", ex.what());
        disconnect();
    }
    catch(const asio::system_error &er)
    {
        spdlog::error("System error: {}", er.what());
        disconnect();
    }
}