#pragma once

#include <stdexcept>
#include <general.pb.h>

struct InvalidMsg : public std::runtime_error
{
    using std::runtime_error::runtime_error;
};

struct InvalidMsgHeader : public InvalidMsg
{
    using InvalidMsg::InvalidMsg;
};

struct InvalidMsgData : public InvalidMsg
{
    using InvalidMsg::InvalidMsg;
};

struct ProtocolError : public std::runtime_error
{
    using std::runtime_error::runtime_error;
};

struct OutOfSequenceMsgReceived : public ProtocolError
{
    using ProtocolError::ProtocolError;
};

struct RequestTimeout : public ProtocolError
{
    using ProtocolError::ProtocolError;
};

struct ErrorMsgReceived
{
    std::string        msg;
    ledcube::ErrorCode code;
};