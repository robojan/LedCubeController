#include <communication/networkconnection.h>
#include <spdlog/spdlog.h>
#include <cassert>
#include "errors.h"
#include <chrono>

using asio::ip::tcp;

NetworkConnection::NetworkConnection(asio::io_context &context, asio::ip::tcp::socket &&socket)
    : _ioContext(context), _socket(std::move(socket)), _seq(1234)
{
}

NetworkConnection::~NetworkConnection()
{
    _socket.shutdown(asio::ip::tcp::socket::shutdown_both);
}

asio::awaitable<std::shared_ptr<NetworkConnection>>
    NetworkConnection::connect(asio::io_context &context, std::string host, uint16_t port)
{
    tcp::resolver resolver(context);

    // Convert the hostname and port to an address
    auto portString = std::to_string(port);
    auto endpoints  = co_await resolver.async_resolve(host, portString, asio::use_awaitable);

    for(auto &e : endpoints)
    {
        spdlog::info("found endpoints: {} {}", e.host_name(), e.endpoint().address().to_string());
    }

    // Try to connect to the
    tcp::socket socket(context);
    auto connectedEndpoint = co_await asio::async_connect(socket, endpoints, asio::use_awaitable);

    spdlog::info("Connected to: {}:{}", connectedEndpoint.address().to_string(),
                 connectedEndpoint.port());

    co_return new NetworkConnection(context, std::move(socket));
}

asio::awaitable<ledcube::Response> NetworkConnection::request(
    std::shared_ptr<ledcube::Request> request,
    int                               timeoutMs)
{
    using clock = std::chrono::high_resolution_clock;
    assert(request);

    auto wp   = weak_from_this();
    auto self = wp.lock();

    // Update the request
    int seq = _seq++;
    request->set_id(seq);

    // Timeout managemant
    auto timer     = asio::high_resolution_timer(_ioContext);
    auto startTime = clock::now();
    auto timeout   = std::chrono::milliseconds(timeoutMs);

    timer.expires_after(timeout);
    timer.async_wait([&](const asio::error_code &ec) {
        if(ec != asio::error::operation_aborted)
            _socket.cancel();
    });

    // Send the request
    co_await sendRequest(*request);
    // Receive the response
    auto response = co_await receiveResponse();
    timer.cancel();

    // Response checking
    if(response.id() != seq)
    {
        throw OutOfSequenceMsgReceived("Message with wrong ID received");
    }
    if(response.has_error())
    {
        throw ErrorMsgReceived{.msg = response.error().msg(), .code = response.error().code()};
    }
    co_return response;
}

asio::awaitable<ledcube::Response> NetworkConnection::request(ledcube::Request &request,
                                                              int               timeoutMs)
{
    using clock = std::chrono::high_resolution_clock;

    // Update the request
    int seq = _seq++;
    request.set_id(seq);

    auto wp   = weak_from_this();
    auto self = wp.lock();

    // Timeout managemant
    auto timer     = asio::high_resolution_timer(_ioContext);
    auto startTime = clock::now();
    auto timeout   = std::chrono::milliseconds(timeoutMs);

    timer.expires_after(timeout);
    timer.async_wait([&](const asio::error_code &ec) {
        if(ec != asio::error::operation_aborted)
            _socket.cancel();
    });

    // Send the request
    co_await sendRequest(request);
    // Receive the response
    auto response = co_await receiveResponse();
    timer.cancel();

    // Response checking
    if(response.id() != seq)
    {
        throw OutOfSequenceMsgReceived("Message with wrong ID received");
    }
    if(response.has_error())
    {
        throw ErrorMsgReceived{.msg = response.error().msg(), .code = response.error().code()};
    }
    co_return response;
}

asio::awaitable<void> NetworkConnection::sendRequest(const ledcube::Request &request)
{
    assert(request.IsInitialized());

    // Send the header
    // TODO: Endian conversion
    MsgHeader header{.magic = cMsgMagic, .length = static_cast<uint32_t>(request.ByteSizeLong())};
    co_await asio::async_write(_socket, asio::const_buffer(&header, sizeof(header)),
                               asio::use_awaitable);

    // Send the data
    std::string msg;
    request.SerializeToString(&msg);
    co_await asio::async_write(_socket, asio::const_buffer(msg.data(), msg.size()),
                               asio::use_awaitable);
}

asio::awaitable<ledcube::Response> NetworkConnection::receiveResponse()
{
    // Receive the header
    // TODO: Endian conversion
    MsgHeader header;
    co_await asio::async_read(_socket, asio::buffer(&header, sizeof(header)), asio::use_awaitable);

    // Check the header
    if(header.magic != cMsgMagic || header.length >= cMaxExpectedMsgLen)
    {
        throw InvalidMsgHeader("Header Corrupt");
    }

    // Receive the message
    std::vector<char> msgData(header.length);
    co_await asio::async_read(_socket, asio::buffer(msgData), asio::use_awaitable);

    // Parse the message
    ledcube::Response response;
    bool              parseResult = response.ParseFromArray(msgData.data(), msgData.size());
    if(!parseResult)
    {
        throw InvalidMsgData("Invalid message");
    }
    // Message parsing good
    co_return response;
}