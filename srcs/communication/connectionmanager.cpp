#include <communication/connectionmanager.h>
#include <spdlog/spdlog.h>
#include <communication/networkconnection.h>
#include <asio.hpp>

ConnectionManager::ConnectionManager() : _ioContextKeepAliveGuard(_ioContext.get_executor())
{
    // Start the execution context
    _ioThread = std::thread([this]() {
        spdlog::info("Starting io thread");
        while(true)
        {
            try
            {
                _ioContext.run();
                break;
            }
            catch(std::exception &e)
            {
                spdlog::critical("Unhandled exception in connection manager: {}", e.what());
                throw e;
            }
        }
        spdlog::info("IO thread stopped");
    });
}

ConnectionManager::~ConnectionManager()
{
    _ioContextKeepAliveGuard.reset();
    // Stop all tasks

    // Remove all the client connections
    _clients.clear();

    // Wait until the executor has finished
    _ioThread.join();
}

void ConnectionManager::connectTo(const std::string &host, uint16_t port)
{
    auto connectCallback = [this](std::exception_ptr                 ex,
                                  std::shared_ptr<NetworkConnection> result) {
        try
        {
            if(ex)
                std::rethrow_exception(ex);
            auto client = std::shared_ptr<LCClient>(new LCClient(_ioContext, std::move(result)));
            client->sigDisconnected.connect(
                std::bind(&ConnectionManager::onClientDisconnected, this, std::placeholders::_1));
            auto serial = client->serial();
            _clients.insert(std::make_pair(serial, client));
            sigClientConnected(serial);

            spdlog::info("connect succeeded");
        }
        catch(asio::system_error &er)
        {
            spdlog::warn("Connect failed: {}", er.what());
        }
    };

    asio::co_spawn(_ioContext.get_executor(), NetworkConnection::connect(_ioContext, host, port),
                   connectCallback);
}

void ConnectionManager::onClientDisconnected(LCClient *client)
{
    auto it = std::find_if(_clients.begin(), _clients.end(),
                           [client](auto &cclient) { return cclient.second.get() == client; });
    if(it == _clients.end())
    {
        spdlog::warn("Unknown client disconnected!");
    }
    spdlog::info("Client disconnected");

    sigClientDisconnected(client->serial());
    _clients.erase(it);
}