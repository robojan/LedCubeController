#pragma once

#include <string>
#include <cstdint>
#include <led-cube.pb.h>
#include <asio.hpp>
#include <memory>

class NetworkConnection : public std::enable_shared_from_this<NetworkConnection>
{
    static constexpr uint32_t cMsgMagic          = 0x4C43f0e2;
    static constexpr uint32_t cMaxExpectedMsgLen = 2048;
    struct MsgHeader
    {
        uint32_t magic;
        uint32_t length;
    };
    static_assert(sizeof(MsgHeader) == 8);

public:
    static constexpr uint16_t cDefaultPort       = 25345;
    static constexpr auto     c_defaultTimeoutMs = 1000;

    static asio::awaitable<std::shared_ptr<NetworkConnection>>
        connect(asio::io_context &context, std::string host, uint16_t port = cDefaultPort);

    ~NetworkConnection();

    asio::awaitable<ledcube::Response> request(std::shared_ptr<ledcube::Request> request,
                                               int timeoutMs = c_defaultTimeoutMs);
    asio::awaitable<ledcube::Response> request(ledcube::Request &request,
                                               int               timeoutMs = c_defaultTimeoutMs);

private:
    asio::io_context &    _ioContext;
    asio::ip::tcp::socket _socket;
    int                   _seq = 0;

    NetworkConnection(asio::io_context &context, asio::ip::tcp::socket &&socket);
    NetworkConnection(const NetworkConnection &) = delete;

    NetworkConnection &operator=(const NetworkConnection &) = delete;

    asio::awaitable<void>              sendRequest(const ledcube::Request &request);
    asio::awaitable<ledcube::Response> receiveResponse();
};
