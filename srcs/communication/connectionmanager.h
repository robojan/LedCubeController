#pragma once

#include <asio.hpp>
#include <thread>
#include <atomic>
#include <unordered_map>
#include <sigslot/signal.hpp>
#include "lcclient.h"

class ConnectionManager
{
public:
    ConnectionManager();
    ~ConnectionManager();

    void connectTo(const std::string &host, uint16_t port);

    int                              numClients() const { return _clients.size(); }
    inline std::shared_ptr<LCClient> operator[](const std::string &serial)
    {
        auto it = _clients.find(serial);
        return it == _clients.end() ? nullptr : it->second;
    }
    auto clientListBegin() const { return _clients.begin(); }
    auto clientListEnd() const { return _clients.end(); }

    // Signals
    sigslot::signal<const std::string &> sigClientConnected;
    sigslot::signal<const std::string &> sigClientDisconnected;

private:
    std::thread                                                _ioThread;
    asio::io_context                                           _ioContext;
    asio::executor_work_guard<asio::io_context::executor_type> _ioContextKeepAliveGuard;
    std::unordered_map<std::string, std::shared_ptr<LCClient>> _clients;

    void onClientDisconnected(LCClient *client);
};