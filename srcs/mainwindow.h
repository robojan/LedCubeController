#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include <size3d.h>
#include <lcframe.h>
#include <communication/connectionmanager.h>
#include <models/connectedclientsmodel.h>

namespace Ui
{
struct MainWindowExtra;
} // namespace Ui

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setupConnections();
    void setupEditor();
    void setupPlaneEditor();
    void setupPreview();
    void setupToolbar();

signals:
    void activePlaneChanged(int plane);
    void activeFrameChanged(int frame);
    void frameDataChanged(int plane = -1); // Plane can be -1 for whole frame update

private:
    Ui::MainWindowExtra *_ui;

    struct
    {
        int activePlane = 0;
    } _editor;
    struct
    {
        int            activeFrame = 0;
        CubeDimensions cubeDimensions{10, 10, 10};
        LCFrame        frameData{10, 10, 10};
    } _general;
    std::shared_ptr<ConnectionManager> _com;
    struct
    {
        ConnectedClientsModel *connectedClients;
    } _models;
};
#endif // MAINWINDOW_H
