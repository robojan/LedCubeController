#pragma once

#include <vector>
#include <QtGlobal>
#include <QColor>

struct LCColor
{
    uint8_t r{0};
    uint8_t g{0};
    uint8_t b{0};

    inline uint32_t       rgb() const { return (r << 16) | (g << 8) | (b << 0); }
    inline QColor         toQColor() const { return QColor(r, g, b); }
    inline static LCColor fromQColor(const QColor &c)
    {
        return LCColor{static_cast<uint8_t>(c.red()), static_cast<uint8_t>(c.green()),
                       static_cast<uint8_t>(c.blue())};
    }
};

inline static bool operator==(const LCColor &a, const LCColor &b)
{
    return a.r == b.r && a.g == b.g && a.b == b.b;
}

inline static bool operator!=(const LCColor &a, const LCColor &b)
{
    return !(a == b);
}

class LCPlane
{
public:
    LCPlane(int rows, int cols);

    inline int rows() const { return _numRows; }
    inline int cols() const { return _numCols; }

    inline LCColor get(int r, int c) const
    {
        Q_ASSERT(r >= 0 && r < _numRows);
        Q_ASSERT(c >= 0 && c < _numCols);
        int idx = r * cols() + c;
        Q_ASSERT(idx < _data.size());
        return _data[idx];
    }
    inline void set(int r, int c, LCColor color)
    {
        Q_ASSERT(r >= 0 && r < _numRows);
        Q_ASSERT(c >= 0 && c < _numCols);
        int idx = r * cols() + c;
        Q_ASSERT(idx < _data.size());
        _data[idx] = color;
    }

    void resize(int r, int c);

private:
    int                  _numRows;
    int                  _numCols;
    std::vector<LCColor> _data;
};

class LCFrame
{
public:
    LCFrame(int rows, int cols, int planes);

    inline int rows() const { return _numRows; }
    inline int cols() const { return _numCols; }
    inline int planes() const { return _planes.size(); }

    LCPlane &operator[](int plane)
    {
        Q_ASSERT(plane >= 0 && plane < _planes.size());
        return _planes[plane];
    }

    inline LCColor get(int r, int c, int p) const
    {
        Q_ASSERT(p >= 0 && p < _planes.size());
        return _planes[p].get(r, c);
    }
    inline void set(int r, int c, int p, LCColor color)
    {
        Q_ASSERT(p >= 0 && p < _planes.size());
        _planes[p].set(r, c, color);
    }

    void resize(int r, int c, int p);

private:
    std::vector<LCPlane> _planes;
    int                  _numRows;
    int                  _numCols;
};