#include "mainwindow.h"

#include <QApplication>
#include <QSurfaceFormat>
#include <spdlog/spdlog.h>
#include <spdlog/async.h>
#include <spdlog/sinks/msvc_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

static void qtMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    const char *file     = context.file ? context.file : "?";
    const char *function = context.function ? context.function : "?";
    int         line     = context.line;
    auto        msgData  = msg.toUtf8();
    switch(type)
    {
    case QtDebugMsg:
        spdlog::debug("[{}:{}] {}", function, line, msgData.constData());
        break;
    case QtInfoMsg:
        spdlog::info("{}", msgData.constData());
        break;
    case QtWarningMsg:
        spdlog::warn("[{}:{}] {}", function, line, msgData.constData());
        break;
    case QtCriticalMsg:
        spdlog::error("[{}:{} {}] {}", file, line, function, msgData.constData());
        break;
    case QtFatalMsg:
        spdlog::critical("[{}:{} {}] {}", file, line, function, msgData.constData());
        std::terminate();
        break;
    }
}

static void initLog()
{
    spdlog::init_thread_pool(8192, 1);
    auto stdout_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    auto stderr_sink = std::make_shared<spdlog::sinks::stderr_color_sink_mt>();
    std::vector<spdlog::sink_ptr> sinks{stdout_sink, stderr_sink};
#ifdef _WIN32
    auto msvc_sink = std::make_shared<spdlog::sinks::msvc_sink_mt>();
    sinks.push_back(msvc_sink);
#endif
    auto logger = std::make_shared<spdlog::async_logger>("logger", sinks.begin(), sinks.end(),
                                                         spdlog::thread_pool(),
                                                         spdlog::async_overflow_policy::block);
    spdlog::register_logger(logger);

    qInstallMessageHandler(qtMessageOutput);

    spdlog::set_level(spdlog::level::debug);
}

int main(int argc, char *argv[])
{
    initLog();

    QApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    QApplication a(argc, argv);
    int          returnCode;
    {
        MainWindow w;
        w.show();
        returnCode = a.exec();
        // Destroy the mainwindow
    }
    spdlog::shutdown();
    return returnCode;
}
