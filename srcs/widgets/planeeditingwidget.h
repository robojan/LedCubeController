#pragma once

#include <QWidget>
#include <QSize>
#include <lcframe.h>

class PlaneEditingWidget : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QSize elementSize READ elementSize WRITE setElementSize)
    Q_PROPERTY(QSize planeSize READ planeSize WRITE setPlaneSize)
    Q_PROPERTY(Tool activeTool READ activeTool WRITE setActiveTool)
    Q_PROPERTY(int spacing READ spacing WRITE setSpacing)
    Q_PROPERTY(QColor activeColor READ activeColor WRITE setActiveColor)

public:
    enum Tool
    {
        Pencil,
        Rectangle,
        Circle,
    };
    Q_ENUM(Tool)

    explicit PlaneEditingWidget(QWidget *parent = nullptr);
    ~PlaneEditingWidget();

    inline QSize          elementSize() const { return _elementSize; }
    inline QSize          planeSize() const { return _planeSize; }
    inline Tool           activeTool() const { return _activeTool; }
    inline int            spacing() const { return _spacing; }
    inline QColor         activeColor() const { return _activeColor; }
    inline const LCPlane &plane() const { return _buffer; }

    void setElementSize(QSize size);
    void setPlaneSize(QSize size);
    void setActiveTool(Tool tool);
    void setSpacing(int spacing);
    void setActiveColor(QColor color);
    void setPlane(const LCPlane &plane);

    QSize sizeHint() const override;

signals:
    void planeChanged() const;

protected:
    void paintEvent(QPaintEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

private:
    QSize   _elementSize{12, 12};
    QSize   _planeSize{2, 2};
    Tool    _activeTool{Pencil};
    int     _spacing{2};
    QColor  _activeColor{QColorConstants::White};
    LCPlane _buffer;
    struct
    {
        // Normalized points
        QPointF p1, p2;
        QPoint  pi1, pi2;
        bool    active = false;
    } _drawing;

    QColor  getLedColor(int r, int c) const;
    QPointF getNormPos(QPointF pos) const;
    QPoint  getLedPos(QPoint pos) const;
    bool    isInDrawingArea(int r, int c) const;
};
