#pragma once

#include <QWidget>
#include <QColor>
#include <QList>

class QColorDialog;

class PaletteWidget : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QColor selectedColor READ selectedColor NOTIFY selectedColorChanged)
    Q_PROPERTY(
        int selectedIndex READ selectedIndex WRITE setSelectedIndex NOTIFY selectedIndexChanged)
    Q_PROPERTY(QVector<QColor> palette READ palette WRITE setPalette NOTIFY paletteChanged)
    Q_PROPERTY(QSize elementSize READ elementSize WRITE setElementSize)
    Q_PROPERTY(int maxColumns READ maxColumns WRITE setMaxColumns)

public:
    PaletteWidget(QWidget *parent = nullptr);
    virtual ~PaletteWidget();

    inline QColor                 selectedColor() const { return _palette[_selectedIndex]; }
    inline int                    selectedIndex() const { return _selectedIndex; }
    inline const QVector<QColor> &palette() const { return _palette; }
    inline QSize                  elementSize() const { return _elementSize; }
    inline int                    maxColumns() const { return _maxNumCols; }

    void setSelectedIndex(int idx);
    void setPalette(QVector<QColor> palette);
    void setElementSize(QSize size);
    void setMaxColumns(int num);

    QSize sizeHint() const override;

signals:
    void selectedColorChanged();
    void selectedIndexChanged();
    void paletteChanged();


protected:
    void paintEvent(QPaintEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;


private:
    int             _selectedIndex = 0;
    int             _maxNumCols    = 10;
    QVector<QColor> _palette{"#000000", "#FFFFFF", "#FF0000", "#00FF00", "#0000FF", "#FF00FF",
                             "#FFFF00", "#00FFFF", "#FF6600", "#66FF00", "#996600", "#669900",
                             "#ff0099", "#0000CC", "#00CC00", "#404040", "#808080", "#C0C0C0",
                             "#009933", "#CCFF00", "#000000", "#000000", "#000000", "#000000",
                             "#000000", "#000000", "#000000", "#000000", "#000000", "#000000"};
    QSize           _elementSize{25, 16};
    QColorDialog *  _colorDialog{};
    int             _editingIdx = -1;

private slots:
    void onColorDialogColorSelected(const QColor &color);
};
