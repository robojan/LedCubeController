#include "planeeditingwidget.h"
#include <QStylePainter>
#include <QPaintEvent>
#include <QColor>
#include <QStyleOption>

PlaneEditingWidget::PlaneEditingWidget(QWidget *parent)
    : QWidget(parent), _buffer(_planeSize.height(), _planeSize.width())
{
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    resize(sizeHint());
}

PlaneEditingWidget::~PlaneEditingWidget() {}

void PlaneEditingWidget::setElementSize(QSize size)
{
    Q_ASSERT(!size.isEmpty());

    if(size != _elementSize)
    {
        _elementSize = size;
        updateGeometry();
    }
}

void PlaneEditingWidget::setPlaneSize(QSize size)
{
    Q_ASSERT(size.isValid());
    if(size != _planeSize)
    {
        _planeSize = size;
        _buffer.resize(_planeSize.height(), _planeSize.width());
        updateGeometry();
    }
}

void PlaneEditingWidget::setActiveTool(Tool tool)
{
    _activeTool = tool;
}

void PlaneEditingWidget::setActiveColor(QColor color)
{
    _activeColor = std::move(color);
}

void PlaneEditingWidget::setSpacing(int spacing)
{
    Q_ASSERT(spacing >= 0);
    if(spacing != _spacing)
    {
        _spacing = spacing;
        updateGeometry();
    }
}

void PlaneEditingWidget::setPlane(const LCPlane &plane)
{
    // Asume the plane has changed
    _buffer = plane;
    emit planeChanged();
    update();
}

QSize PlaneEditingWidget::sizeHint() const
{
    if(_planeSize.isEmpty())
        return QSize(0, 0);
    int w = _planeSize.width();
    int h = _planeSize.height();
    return QSize(w * _elementSize.width() + (w - 1) * _spacing,
                 h * _elementSize.height() + (h - 1) * _spacing);
}

bool PlaneEditingWidget::isInDrawingArea(int r, int c) const
{
    // Get normalized LED position
    QPointF p((c + 0.5f) / _planeSize.width(), (r + 0.5f) / _planeSize.height());

    bool result = false;

    switch(_activeTool)
    {
    case Pencil:
        result = r == _drawing.pi1.y() && c == _drawing.pi1.x();
        break;
    case Rectangle:
    {
        int x1 = _drawing.pi1.x();
        int x2 = _drawing.pi2.x();
        int y1 = _drawing.pi1.y();
        int y2 = _drawing.pi2.y();
        if(x2 < x1)
            std::swap(x1, x2);
        if(y2 < y1)
            std::swap(y1, y2);
        result = c >= x1 && c <= x2 && r >= y1 && r <= y2;
        break;
    }
    case Circle:
    {
        auto  dc  = _drawing.p1 - _drawing.p2;
        auto  dp  = _drawing.p1 - p;
        float rcs = QPointF::dotProduct(dc, dc);
        float rps = QPointF::dotProduct(dp, dp);
        result    = rps < rcs;
        break;
    }
    default:
        break;
    }

    return result;
}

QColor PlaneEditingWidget::getLedColor(int r, int c) const
{
    bool changing = false;
    if(_drawing.active)
    {
        changing = _activeTool != Pencil && isInDrawingArea(r, c);
    }

    return changing ? _activeColor : _buffer.get(r, c).toQColor();
}

void PlaneEditingWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    int   totalHeight      = sizeHint().height();
    qreal devicePixelRatio = painter.device()->devicePixelRatioF();
    Q_ASSERT(_planeSize.width() == _buffer.cols() && _buffer.rows() == _planeSize.height());

    // Draw the led grid
    {
        painter.save();
        auto palette = style()->standardPalette();
        QPen penBorderDark(palette.dark().color());
        QPen penBorderLight(palette.light().color());
        QPen penBorderShadow(palette.shadow().color());
        QPen penBorderMidlight(palette.midlight().color());
        for(int row = 0; row < _planeSize.height(); row++)
        {
            for(int column = 0; column < _planeSize.width(); column++)
            {
                QRect ledRect(
                    column * (_elementSize.width() + _spacing),
                    totalHeight - _elementSize.height() - row * (_elementSize.height() + _spacing),
                    _elementSize.width(), _elementSize.height());

                // Skip drawing of the rectangles that don't need updating
                if(!event->region().intersects(ledRect))
                    continue;

                int x = ledRect.x();
                int y = ledRect.y();
                int w = ledRect.width();
                int h = ledRect.height();
                if(!qFuzzyCompare(devicePixelRatio, qreal(1)))
                {
                    qreal inverseScale = qreal(1) / devicePixelRatio;
                    painter.scale(inverseScale, inverseScale);
                    x = qRound(devicePixelRatio * x);
                    y = qRound(devicePixelRatio * y);
                    w = qRound(devicePixelRatio * w);
                    h = qRound(devicePixelRatio * h);
                }

                // Draw the filling
                painter.fillRect(x + 1, y + 1, w - 3, h - 3, getLedColor(row, column));

                // Draw the border
                QPoint outsideTL[3]{QPoint(x, y + h - 2), QPoint(x, y), QPoint(x + w - 2, y)};
                QPoint outsideBR[3]{QPoint(x, y + h - 1), QPoint(x + w - 1, y + h - 1),
                                    QPoint(x + w - 1, y)};
                QPoint insideTL[3] = {QPoint(x + 1, y + h - 3), QPoint(x + 1, y + 1),
                                      QPoint(x + w - 3, y + 1)};
                QPoint insideBR[3] = {QPoint(x + 1, y + h - 2), QPoint(x + w - 2, y + h - 2),
                                      QPoint(x + w - 2, y + 1)};
                painter.setPen(penBorderDark);
                painter.drawPolyline(outsideTL, 3);
                painter.setPen(penBorderLight);
                painter.drawPolyline(outsideBR, 3);
                painter.setPen(penBorderShadow);
                painter.drawPolyline(insideTL, 3);
                painter.setPen(penBorderMidlight);
                painter.drawPolyline(insideBR, 3);
            }
        }
        painter.restore();
    }
}

QPointF PlaneEditingWidget::getNormPos(QPointF pos) const
{
    auto area = sizeHint();
    // Invert the y axis and scale to normalize the point
    return QPointF(pos.x() / area.width(), (area.height() - pos.y()) / area.height());
}

QPoint PlaneEditingWidget::getLedPos(QPoint pos) const
{
    auto area = sizeHint();
    int  w    = _elementSize.width() + _spacing;
    int  h    = _elementSize.height() + _spacing;

    // Convert x and y to the same direction as row and column. Also apply the offset for the
    // spacing
    int half_spacing = (_spacing + 1) / 2;
    int x            = pos.x() + half_spacing;
    int y            = area.height() - half_spacing - pos.y();

    int c = (x) / w;
    int r = (y) / h;

    return QPoint(c, r);
}

void PlaneEditingWidget::mousePressEvent(QMouseEvent *event)
{
    // Only respond to the left button
    if(event->button() != Qt::LeftButton)
        return;

    QPointF p               = getNormPos(event->localPos());
    QPoint  pi              = getLedPos(event->pos());
    float   halfRoundScaleX = _planeSize.width() * 2;
    float   halfRoundScaleY = _planeSize.height() * 2;
    QPointF halfRoundedP    = QPointF(qRound(p.x() * halfRoundScaleX) / halfRoundScaleX,
                                   qRound(p.y() * halfRoundScaleY) / halfRoundScaleY);

    switch(_activeTool)
    {
    default:
        break;
    case Pencil:
    {
        // Can be outside the buffer if the widget is bigger than the drawing area
        if(!QRectF(0, 0, 1, 1).contains(p))
            break;

        int     r     = pi.y();
        int     c     = pi.x();
        LCColor color = LCColor::fromQColor(_activeColor);

        bool colorChanged = _buffer.get(r, c) != color;
        _buffer.set(r, c, color);
        if(colorChanged)
        {
            emit planeChanged();
        }
        break;
    }
    case Rectangle:
        // Snap to nearest led
        _drawing.active = true;
        _drawing.pi1    = pi;
        _drawing.pi2    = pi;
        break;
    case Circle:
        _drawing.active = true;
        _drawing.p1     = halfRoundedP;
        _drawing.p2     = halfRoundedP;
        break;
    }

    update();
}

void PlaneEditingWidget::mouseReleaseEvent(QMouseEvent *event)
{
    // Only respond to the left button
    if(event->button() != Qt::LeftButton)
        return;

    QPointF p       = getNormPos(event->localPos());
    QPoint  pi      = getLedPos(event->pos());
    _drawing.active = false;

    switch(_activeTool)
    {
    default:
        break;
    case Pencil:
        // The drawing is done in the press event
        break;
    case Rectangle:
    case Circle:
    {
        LCColor color = LCColor::fromQColor(_activeColor);
        for(int r = 0; r < _planeSize.height(); r++)
        {
            for(int c = 0; c < _planeSize.width(); c++)
            {
                if(!isInDrawingArea(r, c))
                    continue;


                _buffer.set(r, c, color);
            }
        }

        emit planeChanged();
        break;
    }
    }
    update();
}

void PlaneEditingWidget::mouseMoveEvent(QMouseEvent *event)
{
    // Only respond to the left button
    if(!(event->buttons() & Qt::LeftButton))
        return;

    QPointF p  = getNormPos(event->localPos());
    QPoint  pi = getLedPos(event->pos());

    if(_drawing.active)
    {
        _drawing.p2  = p;
        _drawing.pi2 = pi;
        update();
    }

    // Can be outside the buffer if the widget is bigger than the drawing area
    if(_activeTool == Pencil && QRectF(0, 0, 1, 1).contains(p))
    {
        int     r     = pi.y();
        int     c     = pi.x();
        LCColor color = LCColor::fromQColor(_activeColor);

        bool colorChanged = _buffer.get(r, c) != color;
        if(colorChanged)
        {
            _buffer.set(r, c, color);
            emit planeChanged();
            update();
        }
    }
}
