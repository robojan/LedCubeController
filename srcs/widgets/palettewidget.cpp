#include "palettewidget.h"
#include <QStyle>
#include <QPainter>
#include <QPaintEvent>
#include <QColorDialog>

PaletteWidget::PaletteWidget(QWidget *parent)
{
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    resize(sizeHint());

    // The color always changes when the index changes
    connect(this, &PaletteWidget::selectedIndexChanged, this, &PaletteWidget::selectedColorChanged);

    _colorDialog = new QColorDialog(this);
    Q_ASSERT(_colorDialog);
    connect(_colorDialog, &QColorDialog::colorSelected, this, &PaletteWidget::onColorDialogColorSelected);
}

PaletteWidget::~PaletteWidget()
{
}

void PaletteWidget::setSelectedIndex(int idx)
{
    Q_ASSERT(idx >= 0 && idx < _palette.size());

    if (idx != _selectedIndex)
    {
        _selectedIndex = idx;
        emit selectedIndexChanged();
        update();
    }
}

void PaletteWidget::setPalette(QVector<QColor> palette)
{
    // Pad the palette to a multiple of maxnumcolumns
    int size = palette.size();
    size = ((size + _maxNumCols - 1) / _maxNumCols) * _maxNumCols;

    int toAdd = size - palette.size();
    while(toAdd--) {
        palette.push_back(QColorConstants::Black);
    }

    if(_selectedIndex >= size) {
        _selectedIndex = 0;
        emit selectedIndexChanged();
    }

    // Close the color dialog if open
    _editingIdx = -1;
    _colorDialog->reject();
    
    bool colorChanged = _palette[_selectedIndex] != palette[_selectedIndex];
    bool sizeChanged = _palette.size() != size;

    // Assume that the palette needs updating
    _palette = std::move(palette);
    emit paletteChanged();

    if(colorChanged) {
        emit selectedColorChanged();
    }
    if(sizeChanged) {
        updateGeometry();
    }
    update();
}

void PaletteWidget::setElementSize(QSize size)
{
    Q_ASSERT(!size.isEmpty());

    if (size != _elementSize)
    {
        _elementSize = size;
        updateGeometry();
    }
}

void PaletteWidget::setMaxColumns(int num)
{
    Q_ASSERT(num > 0);

    if (num != _maxNumCols)
    {
        _maxNumCols = num;
        updateGeometry();
    }
}

QSize PaletteWidget::sizeHint() const
{
    int num = _palette.size();
    int cols = num > _maxNumCols ? _maxNumCols : num;
    int rows = (num + cols - 1) / cols;
    int w = _elementSize.width();
    int h = _elementSize.height();
    return QSize(cols * (1 + w) + 1, rows * (1 + h) + 1);
}

void PaletteWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    int num = _palette.size();
    int cols = num > _maxNumCols ? _maxNumCols : num;
    int rows = (num + cols - 1) / cols;
    QSize size = sizeHint();
    qreal devicePixelRatio = painter.device()->devicePixelRatioF();
    auto palette = style()->standardPalette();
    QPen penGrid(palette.dark().color());
    int w = _elementSize.width();
    int h = _elementSize.height();


    // Draw the grid
    painter.setPen(penGrid);
    for(int r = 0; r <= rows; r++) {
        int x = r * (w + 1);
        painter.drawLine(x, 0, x, size.height());
    }
    for(int c = 0; c <= cols; c++) {
        int y = c * (h + 1);
        painter.drawLine(0, y, size.width(), y);
    }

    // Draw the palette
    QPen highlightPen(palette.highlight().color(), 3);
    painter.setPen(highlightPen);
    for (int r = 0; r < rows; r++)
    {
        for (int c = 0; c < cols; c++)
        {
            int idx = r * cols + c;
            int x = 1 + c * (w + 1);
            int y = 1 + r * (h + 1);
            if (!qFuzzyCompare(devicePixelRatio, qreal(1)))
            {
                qreal inverseScale = qreal(1) / devicePixelRatio;
                painter.scale(inverseScale, inverseScale);
                x = qRound(devicePixelRatio * x);
                y = qRound(devicePixelRatio * y);
                w = qRound(devicePixelRatio * w);
                h = qRound(devicePixelRatio * h);
            }

            QRect rect(x,y,w,h);

            // Skip drawing of the rectangles that don't need updating
            if (!event->region().intersects(rect))
                continue;

            // Draw the filling
            painter.fillRect(rect, _palette[idx]);
            
            // Draw the selected frame
            if(idx == _selectedIndex) {
                painter.drawRect(rect);
            }
        }
    }
}

void PaletteWidget::mousePressEvent(QMouseEvent *event)
{
    int num = _palette.size();
    int cols = num > _maxNumCols ? _maxNumCols : num;
    int rows = (num + cols - 1) / cols;
    int ew = _elementSize.width() + 1;
    int eh = _elementSize.height() + 1;
    QRect area(QPoint(), sizeHint());
    if(!area.contains(event->pos())) 
        return;
    int row = event->y() / eh;
    int col = event->x() / ew;
    int idx = row * cols + col;
    if(idx >= _palette.size()) 
        return;

    switch(event->button()) {
    case Qt::LeftButton:
        // Change selected index
        if(idx == _selectedIndex) 
            break;
        _selectedIndex = idx;
        emit selectedIndexChanged();
        update(); 
        break;
    case Qt::RightButton:
        // Redefine the palette color
        _editingIdx = idx;
        _colorDialog->setCurrentColor(_palette[idx]);
        _colorDialog->open();        
        break;
    default: 
        break;
    }    
}

void PaletteWidget::onColorDialogColorSelected(const QColor &color) {
    Q_ASSERT(_editingIdx >= 0 && _editingIdx < _palette.size());
    
    _palette[_editingIdx] = color;
    if(_editingIdx == _selectedIndex) {
        emit selectedColorChanged();
    }
    update(); 
}