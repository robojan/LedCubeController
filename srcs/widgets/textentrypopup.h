#pragma once

#include <QWidget>

namespace Ui
{
class TextEntryPopup;
}

class TextEntryPopup : public QWidget
{
    Q_OBJECT

public:
    explicit TextEntryPopup(QWidget *parent = nullptr);
    ~TextEntryPopup();

    QString label() const;
    void    setLabel(const QString &label);

    QString acceptButtonText() const;
    void    setAcceptButtonText(const QString &text);

    QString value() const;
    void    setValue(const QString &value);

signals:
    void accepted();
    void valueChanged();

private:
    Ui::TextEntryPopup *ui;
};
