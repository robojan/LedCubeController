#include "ledcubepreviewwidget.h"
#include <QFileInfo>
#include <unordered_map>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QtMath>
#include <algorithm>

LedCubePreviewWidget::LedCubePreviewWidget(QWidget *parent)
{
    QSurfaceFormat format = QSurfaceFormat::defaultFormat();
    format.setDepthBufferSize(24);
    format.setStencilBufferSize(8);
    format.setSamples(4);
    format.setVersion(3, 2);
    format.setProfile(QSurfaceFormat::CoreProfile);
    setFormat(format);
    _plane.setScale({20, 20, 1});
    _plane.setColor(QColor::fromRgbF(0.1f, 0.1f, 0.1f));
    _ledcube.setScale({10, 10, 10});
    _ledcube.setDimensions(10, 10, 10);
    _ledcube.setPosition({0, 0, 1});
}

LedCubePreviewWidget::~LedCubePreviewWidget()
{
    makeCurrent();
    cleanupGL();
    doneCurrent();
}

void LedCubePreviewWidget::updateFrame(const LCFrame &frame)
{
    _ledcube.setFrame(frame);
    update();
}

void LedCubePreviewWidget::updatePlane(int p, const LCPlane &plane)
{
    _ledcube.setPlane(p, plane);
    update();
}

void LedCubePreviewWidget::initializeGL()
{
    initializeOpenGLFunctions();

    glClearColor(0.7f, 0.7f, 0.7f, 1.0f);

    // Enable back face culling
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);

    // Enable depth buffer
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);

    createShaders();
    createCamera();

    auto &program = _renderState.shaderDatabase.get(_plane.shaderProgram());
    program.bind();
    _plane.initialize(_renderState);
    _ledcube.initialize(_renderState);
    program.release();
}

void LedCubePreviewWidget::cleanupGL()
{
    _plane.cleanup(_renderState);
    _ledcube.cleanup(_renderState);
}

void LedCubePreviewWidget::resizeGL(int w, int h)
{
    Q_UNUSED(w);
    Q_UNUSED(h);
    createCamera();
}

void LedCubePreviewWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    auto &program = _renderState.shaderDatabase.get(_plane.shaderProgram());
    program.bind();
    program.setUniformValue("u_mvp", _camera.vpMatrix * _plane.modelMatrix());
    _plane.render(_renderState);
    program.setUniformValue("u_mvp", _camera.vpMatrix * _ledcube.modelMatrix());
    _ledcube.render(_renderState);
    program.release();
}

void LedCubePreviewWidget::createCamera()
{
    float aspectRatio = static_cast<float>(width()) / static_cast<float>(height());
    float r           = _camera.sphericalPos.x();
    float theta       = _camera.sphericalPos.y();
    float phi         = _camera.sphericalPos.z() - M_PI / 2;
    float r2          = r * cos(theta);
    auto  cameraPos   = _camera.targetPos + QVector3D(r2 * cos(phi), r2 * sin(phi), r * sin(theta));

    _camera.vpMatrix.setToIdentity();
    _camera.vpMatrix.perspective(_camera.fov, aspectRatio, 0.5f, 100.0f);
    _camera.vpMatrix.lookAt(cameraPos, _camera.targetPos, {0, 0, 1});
}

void LedCubePreviewWidget::createShaders()
{
    createShader("default", {":/shaders/default.vert.glsl", ":/shaders/default.frag.glsl"});
}

void LedCubePreviewWidget::createShader(QString name, QStringList sources)
{
    static const std::unordered_map<QString, QOpenGLShader::ShaderType> kTypeMap{
        {QStringLiteral("vert.glsl"), QOpenGLShader::Vertex},
        {QStringLiteral("frag.glsl"), QOpenGLShader::Fragment},
        {QStringLiteral("geom.glsl"), QOpenGLShader::Geometry},
        {QStringLiteral("tesc.glsl"), QOpenGLShader::TessellationControl},
        {QStringLiteral("tese.glsl"), QOpenGLShader::TessellationEvaluation},
        {QStringLiteral("comp.glsl"), QOpenGLShader::Compute},
    };
    QPointer<QOpenGLShaderProgram> program{new QOpenGLShaderProgram};
    for(auto &source : sources)
    {
        QFileInfo fileInfo(source);
        QString   ext = fileInfo.completeSuffix();
        auto      it  = kTypeMap.find(ext);

        Q_ASSERT(it != kTypeMap.end());
        auto type = it->second;

        bool success = program->addCacheableShaderFromSourceFile(type, source);
        if(!success)
        {
            qDebug() << "Error adding shader " << source << " : ";
            qDebug() << qPrintable(program->log());
        }
        Q_ASSERT(success);
    }
    bool success = program->link();
    if(!success)
    {
        qDebug() << "Error linking shader" << name << ":";
        qDebug() << qPrintable(program->log());
    }
    Q_ASSERT(success);
    _renderState.shaderDatabase.add(name, std::move(program));
}

void LedCubePreviewWidget::mousePressEvent(QMouseEvent *event)
{
    if(event->button() != Qt::LeftButton)
        return;

    _control.active    = true;
    _control.lastPoint = event->localPos();
}

void LedCubePreviewWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() != Qt::LeftButton)
        return;

    _control.active = false;
}

void LedCubePreviewWidget::mouseMoveEvent(QMouseEvent *event)
{
    if(!_control.active && (event->buttons() & Qt::LeftButton) == 0)
        return;

    auto p             = event->localPos();
    auto dP            = p - _control.lastPoint;
    _control.lastPoint = p;

    static constexpr float minTheta = qDegreesToRadians(-85.0f);
    static constexpr float maxTheta = qDegreesToRadians(85.0f);
    static constexpr float thetaScale =
        (maxTheta - minTheta) / 300;                  // How many pixels for min to max
    static constexpr float phiScale = 2 * M_PI / 600; // How many pixels for min to max

    float theta = std::clamp(_camera.sphericalPos.y() + static_cast<float>(dP.y()) * thetaScale,
                             minTheta, maxTheta);
    float phi   = _camera.sphericalPos.z() - dP.x() * phiScale;

    _camera.sphericalPos.setY(theta);
    _camera.sphericalPos.setZ(phi);
    createCamera();
    update();
}

void LedCubePreviewWidget::wheelEvent(QWheelEvent *event)
{
    auto delta = event->angleDelta().y() / 8.0f; // Wheel angle delta

    static constexpr float minR  = 5;
    static constexpr float maxR  = 50;
    static constexpr float scale = (maxR - minR) / 720; // How many pixels for min to max

    float r = std::clamp(_camera.sphericalPos.x() - delta * scale, minR, maxR);

    _camera.sphericalPos.setX(r);
    createCamera();
    update();
}
