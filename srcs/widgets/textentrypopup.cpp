#include "textentrypopup.h"
#include "ui_textentrypopup.h"

TextEntryPopup::TextEntryPopup(QWidget *parent)
    : QWidget(parent, Qt::Popup), ui(new Ui::TextEntryPopup)
{
    ui->setupUi(this);

    connect(ui->acceptButton, &QToolButton::clicked, this, &TextEntryPopup::accepted);
    connect(ui->valueBox, &QLineEdit::returnPressed, this, &TextEntryPopup::accepted);
    connect(ui->valueBox, &QLineEdit::textChanged, this, &TextEntryPopup::valueChanged);
}

TextEntryPopup::~TextEntryPopup()
{
    delete ui;
}


QString TextEntryPopup::label() const
{
    return ui->label->text();
}

void TextEntryPopup::setLabel(const QString &label)
{
    ui->label->setText(label);
}

QString TextEntryPopup::acceptButtonText() const
{
    return ui->acceptButton->text();
}

void TextEntryPopup::setAcceptButtonText(const QString &text)
{
    ui->acceptButton->setText(text);
}

QString TextEntryPopup::value() const
{
    return ui->valueBox->text();
}

void TextEntryPopup::setValue(const QString &value)
{
    ui->valueBox->setText(value);
}