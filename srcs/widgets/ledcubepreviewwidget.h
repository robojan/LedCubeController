#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QVector3D>
#include <3d/renderstate.h>
#include <3d/objects/plane.h>
#include <3d/objects/ledcube.h>

class LedCubePreviewWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
public:
    explicit LedCubePreviewWidget(QWidget *parent = nullptr);
    virtual ~LedCubePreviewWidget();

    void updateFrame(const LCFrame &frame);
    void updatePlane(int p, const LCPlane &plane);

protected:
    void         initializeGL() override;
    void         resizeGL(int w, int h) override;
    void         paintGL() override;
    virtual void cleanupGL();

    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;

private:
    void createCamera();
    void createShaders();
    void createShader(QString name, QStringList sources);

    struct
    {
        QMatrix4x4 vpMatrix;
        QVector3D  sphericalPos{15, 0.2f, 0};
        QVector3D  targetPos{0, 0, 5.5f};
        float      fov = 60.0f;
    } _camera;
    RenderState      _renderState;
    objects::Plane   _plane;
    objects::LedCube _ledcube;
    struct
    {
        bool    active;
        QPointF lastPoint;
    } _control;
};
