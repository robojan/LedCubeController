#pragma once

#include <QPointer>
#include <QOpenGLShaderProgram>
#include <unordered_map>
#include <QString>

class ShaderDatabase
{
public:
    ShaderDatabase();
    ~ShaderDatabase();

    void                  add(QString name, QPointer<QOpenGLShaderProgram> program);
    QOpenGLShaderProgram &get(const QString &name);

private:
    std::unordered_map<QString, QPointer<QOpenGLShaderProgram>> _db;
};
