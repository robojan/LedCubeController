#include "staticobject.h"
#include "renderstate.h"

StaticObject::StaticObject()
{
    updateModelMatrix();
}

StaticObject::~StaticObject() {}

void StaticObject::initialize(RenderState &s)
{
    initializeOpenGLFunctions();

    _vertexBuffer.obj.create();
    _vao.create();

    // Update the model matrix
    updateModelMatrix();
    // Create the vertex data
    updateVertexData();

    // Get the shader program
    auto &program = s.shaderDatabase.get(shaderProgram());
    {
        program.bind();
        QOpenGLVertexArrayObject::Binder vaoBinder(&_vao);
        Q_ASSERT(_vertexBuffer.stride >= 0);
        Q_ASSERT(_vertexBuffer.stride <= _vertexBuffer.data.size());


        _vertexBuffer.obj.bind();
        for(auto &attr : _vertexBuffer.attributes)
        {
            auto  name     = getAttributeName(attr.first);
            auto &info     = attr.second;
            int   location = program.attributeLocation(name);
            if(location < 0)
            {
                qWarning() << "Cannot find attribute " << name;
            }
            Q_ASSERT(location >= 0);
            program.setAttributeBuffer(location, info.type, info.offset, info.tupleSize,
                                       _vertexBuffer.stride);
            program.enableAttributeArray(location);
        }
        _vertexBuffer.obj.release();
        program.release();
    }
}

void StaticObject::cleanup(RenderState &s) {}

void StaticObject::render(RenderState &s)
{
    Q_ASSERT((_vertexBuffer.data.size() % _vertexBuffer.stride) == 0);
    _vao.bind();

    int numElements = _vertexBuffer.data.size() / _vertexBuffer.stride;
    glDrawArrays(_drawMode, 0, numElements);
    _vao.release();
}

QString StaticObject::shaderProgram() const
{
    return "default";
}

void StaticObject::setPosition(const QVector3D &pos)
{
    if(pos != _position)
    {
        _position = pos;
        updateModelMatrix();
    }
}

void StaticObject::setScale(const QVector3D &scale)
{
    if(scale != _scale)
    {
        _scale = scale;
        updateModelMatrix();
    }
}

void StaticObject::setRotation(const QQuaternion &rotation)
{
    if(rotation != _rotation)
    {
        _rotation = rotation;
        updateModelMatrix();
    }
}

void StaticObject::setPosScaleRot(const QVector3D &  pos,
                                  const QVector3D &  scale,
                                  const QQuaternion &rotation)
{
    _position = pos;
    _scale    = scale;
    _rotation = rotation;
    updateModelMatrix();
}

void StaticObject::updateModelMatrix()
{
    _modelMatrix.setToIdentity();
    _modelMatrix.translate(_position);
    _modelMatrix.scale(_scale);
    _modelMatrix.rotate(_rotation);
}

void StaticObject::updateVertexData() {}

void StaticObject::setVertexBuffer(QByteArray buffer, QOpenGLBuffer::UsagePattern usage)
{
    _vertexBuffer.data = std::move(buffer);
    _vertexBuffer.obj.bind();
    _vertexBuffer.obj.setUsagePattern(usage);
    if(_vertexBuffer.allocated)
    {
        _vertexBuffer.obj.write(0, _vertexBuffer.data.data(), _vertexBuffer.data.size());
    }
    else
    {
        _vertexBuffer.obj.allocate(_vertexBuffer.data.data(), _vertexBuffer.data.size());
    }
    _vertexBuffer.obj.release();
}

void StaticObject::setVertexAttribute(VertexBufferAttribute::Type attribute,
                                      GLenum                      type,
                                      int                         tupleSize,
                                      int                         offset)
{
    _vertexBuffer.attributes[attribute] = VertexBufferAttribute{
        .offset    = offset,
        .type      = type,
        .tupleSize = tupleSize,
    };
}

void StaticObject::remoteVertexAttribute(VertexBufferAttribute::Type attribute)
{
    _vertexBuffer.attributes.erase(attribute);
}

void StaticObject::setVertexDataStride(int stride)
{
    Q_ASSERT(stride >= 0);
    _vertexBuffer.stride = stride;
}

void StaticObject::setDrawMode(GLenum drawMode)
{
    _drawMode = drawMode;
}
