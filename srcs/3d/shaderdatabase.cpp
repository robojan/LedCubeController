#include "shaderdatabase.h"

ShaderDatabase::ShaderDatabase() {}

ShaderDatabase::~ShaderDatabase() {}

#include <QHash>

void ShaderDatabase::add(QString name, QPointer<QOpenGLShaderProgram> program)
{
    _db.insert_or_assign(name, std::move(program));
}

QOpenGLShaderProgram &ShaderDatabase::get(const QString &name)
{
    return *_db.at(name);
}
