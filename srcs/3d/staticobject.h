#pragma once

#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QMatrix4x4>
#include <QVector3D>
#include <QQuaternion>
#include <set>
#include <stdexcept>
#include <QOpenGLFunctions>

class RenderState;

class StaticObject : protected QOpenGLFunctions
{
public:
    StaticObject();
    virtual ~StaticObject();

    virtual void    initialize(RenderState &s);
    virtual void    cleanup(RenderState &s);
    virtual void    render(RenderState &s);
    virtual QString shaderProgram() const;

    inline const QVector3D &  position() const { return _position; }
    inline const QVector3D &  scale() const { return _scale; }
    inline const QQuaternion &rotation() const { return _rotation; }
    inline const QMatrix4x4 & modelMatrix() const { return _modelMatrix; }

    void setPosition(const QVector3D &pos);
    void setScale(const QVector3D &scale);
    void setRotation(const QQuaternion &rotation);
    void setPosScaleRot(const QVector3D &  pos,
                        const QVector3D &  scale    = {1, 1, 1},
                        const QQuaternion &rotation = {});

protected:
    struct VertexBufferAttribute
    {
        enum Type
        {
            Position,
            Color,
            UV,
            Normal,
        };
        int    offset;
        GLenum type;
        int    tupleSize;
    };

    QOpenGLVertexArrayObject _vao;
    QMatrix4x4               _modelMatrix;
    QVector3D                _position{0, 0, 0};
    QVector3D                _scale{1, 1, 1};
    QQuaternion              _rotation;

    void          updateModelMatrix();
    virtual void  updateVertexData();
    void          setVertexBuffer(QByteArray                  buffer,
                                  QOpenGLBuffer::UsagePattern usage = QOpenGLBuffer::StaticDraw);
    void          setVertexAttribute(VertexBufferAttribute::Type attribute,
                                     GLenum                      type,
                                     int                         tupleSize,
                                     int                         offset = 0);
    void          remoteVertexAttribute(VertexBufferAttribute::Type attribute);
    void          setVertexDataStride(int stride);
    void          setDrawMode(GLenum drawMode);
    inline GLenum drawMode() const { return _drawMode; }

    static constexpr const char *getAttributeName(VertexBufferAttribute::Type attr)
    {
        switch(attr)
        {
        case VertexBufferAttribute::Position:
            return "a_position";
        case VertexBufferAttribute::Color:
            return "a_color";
        case VertexBufferAttribute::UV:
            return "a_texcoord";
        case VertexBufferAttribute::Normal:
            return "a_normal";
        default:
            Q_ASSERT(false);
            throw std::invalid_argument("Unsupported vertex attribute");
        }
    }

private:
    struct
    {
        QOpenGLBuffer obj{QOpenGLBuffer::VertexBuffer};
        QByteArray    data;
        int           stride = 0;
        std::unordered_map<VertexBufferAttribute::Type, VertexBufferAttribute> attributes;
        bool                                                                   allocated = false;
    } _vertexBuffer;
    GLenum _drawMode = GL_TRIANGLES;
};
