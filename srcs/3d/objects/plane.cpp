#include "plane.h"
#include <3d/renderstate.h>
#include <QByteArray>

using namespace objects;

Plane::Plane() {}

Plane::~Plane() {}

void Plane::initialize(RenderState &s)
{
    StaticObject::initialize(s);
}

void Plane::cleanup(RenderState &s)
{
    StaticObject::cleanup(s);
}

void Plane::render(RenderState &s)
{
    if(_updateNeeded)
    {
        updateVertexData();
    }
    StaticObject::render(s);
}

void Plane::setColor(const QColor &color)
{
    if(color != _color)
    {
        _color        = color;
        _updateNeeded = true;
    }
}

void Plane::updateVertexData()
{
    static constexpr int stride       = 3 * sizeof(float) + 4 * sizeof(float);
    static constexpr int numTriangles = 2;
    static constexpr int numVertices  = numTriangles * 3;
    static constexpr int bufferSize   = numVertices * stride;

    QByteArray buf(bufferSize, Qt::Uninitialized);
    auto       posPtr   = reinterpret_cast<float *>(buf.data() + 0);
    auto       colorPtr = reinterpret_cast<float *>(buf.data() + 3 * sizeof(float));

    auto addVertex = [&](float x, float y) {
        posPtr[0]   = x;
        posPtr[1]   = y;
        posPtr[2]   = 0;
        colorPtr[0] = _color.redF();
        colorPtr[1] = _color.greenF();
        colorPtr[2] = _color.blueF();
        colorPtr[3] = _color.alphaF();
        posPtr      = reinterpret_cast<float *>(reinterpret_cast<char *>(posPtr) + stride);
        colorPtr    = reinterpret_cast<float *>(reinterpret_cast<char *>(colorPtr) + stride);
    };

    // Add vertices
    addVertex(0.5f, 0.5f);
    addVertex(-0.5f, 0.5f);
    addVertex(-0.5f, -0.5f);
    addVertex(0.5f, 0.5f);
    addVertex(-0.5f, -0.5f);
    addVertex(0.5f, -0.5f);

    // Check if the logic was correct
    Q_ASSERT(reinterpret_cast<char *>(posPtr) - buf.data() == buf.size());

    setVertexBuffer(buf, QOpenGLBuffer::StreamDraw);
    setVertexDataStride(stride);
    setVertexAttribute(VertexBufferAttribute::Position, GL_FLOAT, 3, 0);
    setVertexAttribute(VertexBufferAttribute::Color, GL_FLOAT, 4, 3 * sizeof(float));
}