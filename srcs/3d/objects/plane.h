#pragma once

#include <3d/staticobject.h>
#include <QColor>
#include <atomic>

namespace objects
{
class Plane : public StaticObject
{
public:
    Plane();
    virtual ~Plane();

    void initialize(RenderState &s) override;
    void cleanup(RenderState &s) override;
    void render(RenderState &s) override;

    inline QColor color() const { return _color; }
    void          setColor(const QColor &color);

protected:
    QColor           _color        = QColorConstants::Red;
    std::atomic_bool _updateNeeded = false;

    void updateVertexData() override;
};

} // namespace objects
