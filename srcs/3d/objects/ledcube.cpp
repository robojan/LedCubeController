#include "ledcube.h"
#include <algorithm>
#include <QtMath>
#include <QVector3D>
#include <QVector2D>
#include <3d/renderstate.h>

using namespace objects;

LedCube::LedCube() {}

void LedCube::setNumRows(int r)
{
    Q_ASSERT(r > 0);
    _numRows = r;
    _frame.resize(_numRows, _numCols, _numPlanes);

    _updateNeeded = true;
}

void LedCube::setNumCols(int c)
{
    Q_ASSERT(c > 0);
    _numCols = c;
    _frame.resize(_numRows, _numCols, _numPlanes);

    _updateNeeded = true;
}

void LedCube::setNumPlanes(int p)
{
    Q_ASSERT(p > 0);
    _numPlanes = p;
    _frame.resize(_numRows, _numCols, _numPlanes);

    _updateNeeded = true;
}

void LedCube::setDimensions(int r, int c, int p)
{
    Q_ASSERT(p > 0);
    Q_ASSERT(r > 0);
    Q_ASSERT(c > 0);
    _numCols   = c;
    _numRows   = r;
    _numPlanes = p;
    _frame.resize(_numRows, _numCols, _numPlanes);

    _updateNeeded = true;
}

void LedCube::setBackColor(QColor color)
{
    Q_ASSERT(color.isValid());
    _backColor    = std::move(color);
    _updateNeeded = true;
}

void LedCube::setFrame(const LCFrame &frame)
{
    _frame        = frame;
    _updateNeeded = true;
}

void LedCube::setPlane(int p, const LCPlane &plane)
{
    Q_ASSERT(plane.cols() == _frame.cols());
    Q_ASSERT(plane.rows() == _frame.rows());
    _frame[p]     = plane;
    _updateNeeded = true;
}

void LedCube::updateVertexData()
{
    // Constants
    static constexpr int stride             = 3 * sizeof(float) + 4 * sizeof(float);
    static constexpr int numTrianglesPerLED = 12;
    static constexpr int numVerticesPerLED  = numTrianglesPerLED * 3;
    int                  numLEDS            = 2 * _numRows * _numCols * _numPlanes;
    int                  maxDimension       = std::max(_numRows, std::max(_numCols, _numPlanes));
    float                scale              = 1.0f / maxDimension;
    int                  vertexBufferSize   = numLEDS * numVerticesPerLED * stride;

    // Create the vertex data buffer.
    QByteArray vertexData(vertexBufferSize, Qt::Uninitialized);
    float *    posData = reinterpret_cast<float *>(vertexData.data() + 0);
    float *    colData = reinterpret_cast<float *>(vertexData.data() + 3 * sizeof(float));

    // Define helper functions.
    [[maybe_unused]] int ledsAdded = 0; // Safety check
    auto createLED = [&](const QVector3D &position, bool back, const QColor &color) {
        static constexpr float lw = 0.1f;
        static constexpr float lh = 0.1f;
        static constexpr float ld = 0.025f;
        // Naming convention: [Bottom/Top, Back/Front, Left/Right]
        static constexpr QVector3D BBL(-lw / 2, 0, 0);
        static constexpr QVector3D BBR(lw / 2, 0, 0);
        static constexpr QVector3D BFL(-lw / 2, -ld, 0);
        static constexpr QVector3D BFR(lw / 2, -ld, 0);
        static constexpr QVector3D TBL(-lw / 2, 0, lh);
        static constexpr QVector3D TBR(lw / 2, 0, lh);
        static constexpr QVector3D TFL(-lw / 2, -ld, lh);
        static constexpr QVector3D TFR(lw / 2, -ld, lh);
        auto                       t = [&](const QVector3D &x) {
            auto basePos = (back ? QVector3D(-x.x(), -x.y(), x.z()) : x);
            return (basePos + position) * scale;
        };
        auto setColor = [&](const QVector3D &pos, const QColor &c) {
            auto tp    = t(pos);
            posData[0] = tp.x();
            posData[1] = tp.y();
            posData[2] = tp.z();
            colData[0] = c.redF();
            colData[1] = c.greenF();
            colData[2] = c.blueF();
            colData[3] = c.alphaF();
            posData    = reinterpret_cast<float *>(reinterpret_cast<char *>(posData) + stride);
            colData    = reinterpret_cast<float *>(reinterpret_cast<char *>(colData) + stride);
        };

        auto set = [&](const QVector3D &pos) { setColor(pos, color); };
        Q_ASSERT(ledsAdded < numLEDS);

        // clang-format off
        // Add the triangles
        set(TBR); set(TBL); set(TFL);
        set(TBR); set(TFL); set(TFR);
        set(TFL); set(TBL); set(BBL);
        set(TFL); set(BBL); set(BFL);
        set(TFR); set(TFL); set(BFL);
        set(TFR); set(BFL); set(BFR);
        set(TBR); set(TFR); set(BFR);
        set(TBR); set(BFR); set(BBR);
        set(BFR); set(BFL); set(BBL);
        set(BFR); set(BBL); set(BBR);
        // Add the back side triangle
        setColor(BBR, _backColor); setColor(BBL, _backColor); setColor(TBL, _backColor);
        setColor(BBR, _backColor); setColor(TBL, _backColor); setColor(TBR, _backColor);
        // clang-format on

        ledsAdded++;
    };

    auto createPair = [&](int r, int c, int p) {
        QVector3D position(-(_numCols - 1.0f) / 2 + c, -(_numRows - 1.0f) / 2 + r, p);
        auto      offset = QVector3D(0, -0.05f, 0);
        auto      color  = _frame.get(r, c, p).toQColor();
        createLED(position + offset, false, color);
        createLED(position - offset, true, color);
    };

    // Update the data
    for(int p = 0; p < _numPlanes; p++)
    {
        for(int r = 0; r < _numRows; r++)
        {
            for(int c = 0; c < _numCols; c++)
            {
                createPair(r, c, p);
            }
        }
    }
    Q_ASSERT(reinterpret_cast<char *>(posData) - vertexData.data() == vertexData.size());
    posData = reinterpret_cast<float *>(vertexData.data());

    setVertexBuffer(vertexData, QOpenGLBuffer::StreamDraw);
    setVertexDataStride(stride);

    setVertexAttribute(VertexBufferAttribute::Position, GL_FLOAT, 3, 0);
    setVertexAttribute(VertexBufferAttribute::Color, GL_FLOAT, 4, 3 * sizeof(float));
}

void LedCube::initialize(RenderState &s)
{
    StaticObject::initialize(s);
}

void LedCube::cleanup(RenderState &s)
{
    StaticObject::cleanup(s);
}

void LedCube::render(RenderState &s)
{
    if(_updateNeeded)
    {
        updateVertexData();
    }
    StaticObject::render(s);
}