#pragma once

#include <3d/staticobject.h>
#include <atomic>
#include <QColor>
#include <lcframe.h>

namespace objects
{
class LedCube : public StaticObject
{
public:
    LedCube();
    virtual ~LedCube() = default;

    void setNumRows(int r);
    void setNumCols(int c);
    void setNumPlanes(int p);
    void setDimensions(int r, int c, int p);
    void setBackColor(QColor color);
    void setFrame(const LCFrame &frame);
    void setPlane(int p, const LCPlane &plane);

    inline int    numRows() const { return _numRows; }
    inline int    numCols() const { return _numCols; }
    inline int    numPlanes() const { return _numPlanes; }
    inline QColor backColor() const { return _backColor; }

    void initialize(RenderState &s) override;
    void cleanup(RenderState &s) override;
    void render(RenderState &s) override;

protected:
    std::atomic_bool _updateNeeded = false;
    int              _numRows      = 1;
    int              _numCols      = 1;
    int              _numPlanes    = 1;
    QColor           _backColor    = QColorConstants::Black;
    LCFrame          _frame{1, 1, 1};

    void updateVertexData() override;
};

} // namespace objects
