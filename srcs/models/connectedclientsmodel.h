#pragma once

#include <QAbstractListModel>
#include <memory>
#include <sigslot/signal.hpp>
#include <unordered_set>

class ConnectionManager;

class ConnectedClientsModel : public QAbstractListModel
{
public:
    ConnectedClientsModel(std::shared_ptr<ConnectionManager> mngr, QObject *parent = nullptr);
    ~ConnectedClientsModel();

    int      rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int             section,
                        Qt::Orientation orientation,
                        int             role = Qt::DisplayRole) const override;

private slots:
    void onClientAdded(const std::string &serial);
    void onClientRemoved(const std::string &serial);

private:
    std::shared_ptr<ConnectionManager> _mngr;
    std::vector<std::string>           _clientMap;
};
