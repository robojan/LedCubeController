#include "connectedclientsmodel.h"
#include <communication/connectionmanager.h>
#include <algorithm>

ConnectedClientsModel::ConnectedClientsModel(std::shared_ptr<ConnectionManager> mngr,
                                             QObject *                          parent)
    : QAbstractListModel(parent), _mngr(std::move(mngr))
{
    Q_ASSERT(_mngr);
    _mngr->sigClientConnected.connect(&ConnectedClientsModel::onClientAdded, this);
    _mngr->sigClientDisconnected.connect(&ConnectedClientsModel::onClientRemoved, this);
    std::transform(_mngr->clientListBegin(), _mngr->clientListEnd(), std::back_inserter(_clientMap),
                   [](auto &x) { return x.second->serial(); });
}

ConnectedClientsModel::~ConnectedClientsModel() {}

int ConnectedClientsModel::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid())
        return 0;
    return _clientMap.size();
}

QVariant ConnectedClientsModel::data(const QModelIndex &index, int role) const
{
    QVariant result;
    if(!index.isValid())
        return result;

    int r = index.row();
    if(r < 0 || r >= _clientMap.size())
        return result;
    auto &serial = _clientMap[r];
    auto  client = (*_mngr)[serial];
    if(!client)
        return result;

    switch(role)
    {
    case Qt::DisplayRole:
        result = QString::fromStdString(serial);
        break;
    default:
        break;
    }
    return result;
}

QVariant ConnectedClientsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    QVariant result;
    if(orientation == Qt::Vertical)
    {
        switch(role)
        {
        case Qt::DisplayRole:
            result = QString::number(section + 1);
            break;
        default:
            break;
        }
    }
    else
    {
        switch(role)
        {
        case Qt::DisplayRole:
            result = tr("Serial");
            break;
        default:
            break;
        }
    }
    return result;
}

void ConnectedClientsModel::onClientAdded(const std::string &serial)
{
    auto biggerIt     = std::upper_bound(_clientMap.begin(), _clientMap.end(), serial);
    int  insertionIdx = std::distance(_clientMap.begin(), biggerIt);
    emit beginInsertRows(QModelIndex(), insertionIdx, insertionIdx);
    _clientMap.insert(biggerIt, serial);
    emit endInsertRows();
}

void ConnectedClientsModel::onClientRemoved(const std::string &serial)
{
    auto it = std::find(_clientMap.begin(), _clientMap.end(), serial);
    if(it == _clientMap.end())
        return;
    int  removalPos = std::distance(_clientMap.begin(), it);
    emit beginRemoveRows(QModelIndex(), removalPos, removalPos);
    _clientMap.erase(it);
    emit endInsertRows();
}