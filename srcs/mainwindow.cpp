#include "mainwindow.h"
#include <mainwindowextra.h>
#include <QComboBox>
#include <QRegularExpression>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), _ui(new Ui::MainWindowExtra)
{
    _ui->setupUi(this);
    _com = std::make_shared<ConnectionManager>();
    setupConnections();
}

MainWindow::~MainWindow()
{
    delete _ui;
}

void MainWindow::setupConnections()
{
    setupEditor();
    setupPreview();
    setupToolbar();
}

void MainWindow::setupEditor()
{
    setupPlaneEditor();
}

void MainWindow::setupPlaneEditor()
{
    // Connect the active plane logic
    connect(_ui->planeIncBtn, &QToolButton::clicked, [this](bool checked) {
        Q_UNUSED(checked);
        if(_editor.activePlane < _general.cubeDimensions.planes - 1)
        {
            _editor.activePlane++;
            emit activePlaneChanged(_editor.activePlane);
        }
    });
    connect(_ui->planeDecBtn, &QToolButton::clicked, [this](bool checked) {
        Q_UNUSED(checked);
        if(_editor.activePlane > 0)
        {
            _editor.activePlane--;
            emit activePlaneChanged(_editor.activePlane);
        }
    });
    auto updateActivePlaneLabel = [this](int plane) {
        _ui->planeIndLbl->setText(QStringLiteral("%1").arg(plane + 1));
    };
    connect(this, &MainWindow::activePlaneChanged, updateActivePlaneLabel);
    updateActivePlaneLabel(_editor.activePlane);
    connect(this, &MainWindow::activePlaneChanged,
            [this](int plane) { _ui->planeEditor->setPlane(_general.frameData[plane]); });

    // Update the planeeditor
    _ui->planeEditor->setPlaneSize(
        QSize(_general.cubeDimensions.columns, _general.cubeDimensions.rows));
    _ui->planeEditor->setActiveColor(_ui->palette->selectedColor());

    connect(_ui->toolPencilBtn, &QToolButton::clicked, [this](bool checked) {
        Q_UNUSED(checked);
        _ui->planeEditor->setActiveTool(PlaneEditingWidget::Pencil);
    });
    connect(_ui->toolCircleBtn, &QToolButton::clicked, [this](bool checked) {
        Q_UNUSED(checked);
        _ui->planeEditor->setActiveTool(PlaneEditingWidget::Circle);
    });
    connect(_ui->toolSquareBtn, &QToolButton::clicked, [this](bool checked) {
        Q_UNUSED(checked);
        _ui->planeEditor->setActiveTool(PlaneEditingWidget::Rectangle);
    });
    connect(_ui->planeEditor, &PlaneEditingWidget::planeChanged, [this]() {
        _general.frameData[_editor.activePlane] = _ui->planeEditor->plane();
        emit frameDataChanged(_editor.activePlane);
    });

    // Update the palette
    connect(_ui->palette, &PaletteWidget::selectedColorChanged,
            [this]() { _ui->planeEditor->setActiveColor(_ui->palette->selectedColor()); });
}

void MainWindow::setupPreview()
{
    connect(this, &MainWindow::frameDataChanged, [this](int plane) {
        if(plane < 0)
        {
            // Update the whole frame
            _ui->previewWidget->updateFrame(_general.frameData);
        }
        else
        {
            _ui->previewWidget->updatePlane(plane, _general.frameData[plane]);
        }
    });
}

void MainWindow::setupToolbar()
{
    _models.connectedClients = new ConnectedClientsModel(_com, this);
    _ui->deviceBox->setModel(_models.connectedClients);

    connect(_ui->actionAddDevice, &QAction::triggered, [this]() {
        auto pos = _ui->deviceBox->mapToGlobal(QPoint(0, 0));
        pos.setY(pos.y() + _ui->deviceBox->height());
        _ui->networkPopup->move(pos);
        _ui->networkPopup->show();
    });
    connect(_ui->networkPopup, &TextEntryPopup::accepted, [this]() {
        auto               value = _ui->networkPopup->value();
        QRegularExpression r(
            "^\\s*(?|(?:\\[([^\\]]+)\\](?::(\\d+))?)|(?:([^\\s\\\\\\/:#?&]+)(?::(\\d+))?))\\s*$");
        auto m = r.match(value);
        if(m.hasMatch())
        {
            auto host         = m.captured(1);
            auto port         = m.lastCapturedIndex() > 1 ? m.captured(2) : QStringLiteral("25345");
            bool isPortNumber = false;
            auto portNr       = port.toLong(&isPortNumber);
            if(isPortNumber && portNr > 0 && portNr <= UINT16_MAX)
            {
                qInfo() << "Found match: " << host << " port " << port;
                _com->connectTo(host.toStdString(), portNr);
                _ui->networkPopup->hide();
                return;
            }
        }
        qWarning() << "Invalid resource address";
    });
}