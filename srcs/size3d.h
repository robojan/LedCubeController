#ifndef SIZE3D_H
#define SIZE3D_H

#include <QObject>

class Size3D
{
    Q_GADGET
    Q_PROPERTY(int x MEMBER x)
    Q_PROPERTY(int y MEMBER y)
    Q_PROPERTY(int z MEMBER z)
public:
    inline Size3D(int x, int y, int z) : x(x), y(y), z(z) {}
    Size3D() = default;

    int x = 0;
    int y = 0;
    int z = 0;
};
Q_DECLARE_METATYPE(Size3D)

class CubeDimensions
{
    Q_GADGET
    Q_PROPERTY(int rows MEMBER rows)
    Q_PROPERTY(int columns MEMBER columns)
    Q_PROPERTY(int planes MEMBER planes)
public:
    inline CubeDimensions(int rows, int columns, int planes)
        : rows(rows), columns(columns), planes(planes)
    {
    }
    CubeDimensions() = default;

    int rows    = 0;
    int columns = 0;
    int planes  = 0;
};
Q_DECLARE_METATYPE(CubeDimensions)

#endif // SIZE3D_H
