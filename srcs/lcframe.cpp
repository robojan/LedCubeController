#include "lcframe.h"
#include <algorithm>

// Plane
LCPlane::LCPlane(int r, int c) : _numRows(r), _numCols(c), _data(r * c, {0, 0, 0})
{
    Q_ASSERT(r > 0 && c > 0);
}

void LCPlane::resize(int r, int c)
{
    Q_ASSERT(r > 0 && c > 0);
    _data.resize(r * c, {0, 0, 0});
    _data.shrink_to_fit();
    std::fill(_data.begin(), _data.end(), LCColor{0, 0, 0});
    _numCols = c;
    _numRows = r;
}

// Frame

LCFrame::LCFrame(int r, int c, int p) : _numRows(r), _numCols(c), _planes(p, {r, c})
{
    Q_ASSERT(r > 0 && c > 0 && p > 0);
}

void LCFrame::resize(int r, int c, int p)
{
    Q_ASSERT(r > 0 && c > 0 && p > 0);
    _planes.resize(p, {r, c});
    _planes.shrink_to_fit();
    for(auto &p : _planes)
    {
        p.resize(r, c);
    }
    _numCols = c;
    _numRows = r;
}